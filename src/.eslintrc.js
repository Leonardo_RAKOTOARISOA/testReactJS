module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: ['react', '@typescript-eslint', 'import', 'simple-import-sort'],
    rules: {
        '@typescript-eslint/no-var-requires': 'off',
        'import/no-extraneous-dependencies': 'off',
        'react/require-default-props': 'off',
        'no-plusplus': 'off',
        'react/jsx-props-no-spreading': 'off',
        'react/jsx-filename-extension': [1, { extensions: ['.tsx', '.ts'] }],
        camelcase: 'off',
        'import/prefer-default-export': 'off',
        'react/react-in-jsx-scope': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        'no-nested-ternary': 'off',
        'import/no-unresolved': 'warn',
        'no-restricted-globals': 'off',
        'consistent-return': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
        'no-use-before-define': ['error', { functions: false, classes: false, variables: false }],
        '@typescript-eslint/no-unused-vars': ['warn', { ignoreRestSiblings: true }],
        // 'react-hooks/exhaustive-deps': 'off',
        'no-unused-vars': 'warn',
        'jsx-a11y/click-events-have-key-events': 'off',
        'jsx-a11y/no-static-element-interactions': 'off',
        'import/extensions': [
            'error',
            'ignorePackages',
            {
                js: 'never',
                jsx: 'never',
                ts: 'never',
                tsx: 'never',
            },
        ],
        'react/jsx-boolean-value': ['error', 'always'],
        'sort-imports': 'off',
        'import/order': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        'simple-import-sort/imports': [
            'warn',
            {
                groups: [['^\\u0000', '^@?\\w', '^[^.]', '^\\.']],
            },
        ],
        'simple-import-sort/exports': 'error',
    },
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
            },
        },
        react: {
            version: 'detect', // Tells eslint-plugin-react to automatically detect the version of React to use
        },
    },
    overrides: [
        {
            files: ['**/*.tsx'],
            rules: {
                'react/prop-types': 'off',
            },
        },
    ],
};
