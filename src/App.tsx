import { ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter } from 'react-router-dom';
import { myTheme } from "@Themes";
import Router from '@Route'

function App() {
  return (
    <ThemeProvider theme={myTheme}>
      <BrowserRouter>
        <Router />
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
