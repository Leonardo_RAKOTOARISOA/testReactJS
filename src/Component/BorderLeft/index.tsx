import React from "react";
import "../../styles/border.scss";
import { Container, Typography } from "@material-ui/core";
import { makeStyles, Theme } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { useHistory } from "react-router-dom";
import { useRessource } from "@Hooks";

const useStyles = makeStyles((theme: Theme) => ({
  bdColor: {
    border: "solid 2px transparent",
    width: "230px",
  },
}));

function BorderLeft(props: any) {
  const classes = useStyles();
  const history = useHistory();
  const datas = props.resource;

  const { callOneRess } = useRessource();

  const redirect = (link: string) => () => {
    callOneRess(datas.id);
    history.push(link);
  };

  return (
    <Container onClick={redirect("/detailRessource")} className="root">
      <Box
        className={classes.bdColor}
        style={{ borderLeftColor: `${datas.color}` }}
      >
        <Typography className="h4">
          {datas.name} ({datas.pantone_value})
        </Typography>
        <Typography className="h1">{datas.year}</Typography>
      </Box>
    </Container>
  );
}

export default BorderLeft;
