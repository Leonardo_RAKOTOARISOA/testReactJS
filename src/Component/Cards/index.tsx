import React, { useEffect } from "react";
import "../../styles/card.scss";
import Card from "@material-ui/core/Card";
import { useListe } from "@Hooks";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router-dom";

const Cards = (props: any) => {
  const history = useHistory();
  const datas = props.liste;

  const { callData } = useListe();

  const redirect = (link: string) => () => {
    callData(datas.id);
    history.push(link);
  };

  return (
    <>
      <Card onClick={redirect("/detail")} className="root">
        <CardContent className="position">
          <img className="avatar" alt="Remy Sharp" src={datas.avatar} />
          <Typography variant="h5" component="h2">
            {datas.first_name} {datas.last_name}
          </Typography>
          <Typography className="pos" color="textSecondary">
            {datas.email}
          </Typography>
        </CardContent>
      </Card>
    </>
  );
};

export default Cards;
