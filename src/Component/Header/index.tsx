import React, { FC, useEffect } from "react";
import Cards from "../Cards";
import BorderLeft from "../BorderLeft";
import "../../styles/header.scss";
import { makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tab from "@material-ui/core/Tab";
import TabContext from "@material-ui/lab/TabContext";
import TabList from "@material-ui/lab/TabList";
import TabPanel from "@material-ui/lab/TabPanel";
import Grid from "@material-ui/core/Grid";
import Filtre from "../Filtre";
import { useListe, useRessource } from "@Hooks";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    "& .MuiTab-root": {
      textTransform: "none",
    },
  },
  appbar: {
    backgroundColor: "white",
    color: "black",
    display: "grid",
    justifyContent: "end",

    "& .PrivateTabIndicator-colorSecondary-5": {
      backgroundColor: "black",
    },
  },
}));

const Header = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState("1");

  const handleChange = (event: React.ChangeEvent<any>, newValue: string) => {
    setValue(newValue);
  };

  const { list, callAxios } = useListe();
  const { listRes, ressourceAxios } = useRessource();

  useEffect(() => {
    callAxios();
    ressourceAxios();
  }, []);

  return (
    <div className={classes.root}>
      <TabContext value={value}>
        <AppBar position="static" className={classes.appbar}>
          <TabList onChange={handleChange} aria-label="simple tabs example">
            <Tab label="Liste des Utilisateurs" value="1" />
            <Tab label="Liste des ressources" value="2" />
          </TabList>
        </AppBar>
        <TabPanel value="1">
          <Grid container={true} direction="row" spacing={2}>
            {list.map((liste: any) => {
              return (
                <Grid item={true} md={3} sm={6} xs={12}>
                  <Cards liste={liste} />
                </Grid>
              );
            })}
          </Grid>
        </TabPanel>
        <TabPanel value="2">
          <Filtre />
          <Grid container={true} direction="row" spacing={2}>
            {listRes.map((resource: any) => {
              return (
                <Grid item={true} md={3} sm={6} xs={12}>
                  <BorderLeft resource={resource} />
                </Grid>
              );
            })}
          </Grid>
        </TabPanel>
      </TabContext>
    </div>
  );
};

export default Header;
