/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
/* eslint-disable function-paren-newline, no-extra-boolean-cast */
import { AlertActions } from '@Actions';
import { appAlert } from '@Reducers';
import { useDispatch, useSelector } from 'react-redux';

type TypeSeverity = 'success' | 'info' | 'warning' | 'error';

interface dataApi {
    open: boolean;
    message: string;
    severity: TypeSeverity
}

const useAlert = () => {

    const dispatch = useDispatch();

    const data: dataApi = useSelector(appAlert);

    const setOpenAlert = (open: boolean, message: string, severity: TypeSeverity) => {
        dispatch(AlertActions.setMessage(message));
        dispatch(AlertActions.setOpen(open));
        dispatch(AlertActions.setSeverity(severity));
    };

    const setCloseAlert = () => {
        dispatch(AlertActions.setOpen(false));
        dispatch(AlertActions.setMessage(""));
        dispatch(AlertActions.setSeverity('error'));
    };

    return {
        ...data,
        setOpenAlert,
        setCloseAlert
    };
};

export default useAlert;
