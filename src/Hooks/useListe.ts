/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
/* eslint-disable function-paren-newline, no-extra-boolean-cast */
import axios from "axios";
import { ListActions } from "@Actions";
import { appList } from "@Reducers";
import { useDispatch, useSelector } from "react-redux";

const useListe = () => {
  const callAxios = async () => {
    axios.get(`https://reqres.in/api/users?page=2`).then((res) => {
      setList(res.data);
    });
  };

  const callData = async (id: number) => {
    axios.get(`https://reqres.in/api/users/${id}`).then((res) => {
      setOneUser(res.data);
    });
  };

  const dispatch = useDispatch();

  const data: any = useSelector(appList);

  const setList = (data: any) => {
    dispatch(ListActions.setList(data));
  };

  const setOneUser = (data: any) => {
    dispatch(ListActions.setOneUser(data));
  };

  return {
    ...data,
    setList,
    callAxios,
    setOneUser,
    callData,
  };
};

export default useListe;
