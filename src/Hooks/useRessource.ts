/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
/* eslint-disable function-paren-newline, no-extra-boolean-cast */
import axios from "axios";
import { RessourceActions } from "@Actions";
import { appRessource } from "@Reducers";
import { useDispatch, useSelector } from "react-redux";

const useRessource = () => {
  const ressourceAxios = async () => {
    axios.get(`https://reqres.in/api/unknown`).then((res) => {
      setListRess(res.data);
    });
  };

  const callOneRess = async (id: number) => {
    axios.get(`https://reqres.in/api/unknown/${id}`).then((res) => {
      setOneRessource(res.data);
    });
  };

  const dispatch = useDispatch();

  const data: any = useSelector(appRessource);

  const setListRess = (data: any) => {
    dispatch(RessourceActions.setListRess(data));
  };

  const setOneRessource = (data: any) => {
    dispatch(RessourceActions.setOneRessource(data));
  };

  return {
    ...data,
    setListRess,
    ressourceAxios,
    setOneRessource,
    callOneRess,
  };
};

export default useRessource;
