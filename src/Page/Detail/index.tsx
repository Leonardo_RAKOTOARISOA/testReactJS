import "../../styles/detail.scss";
import { useListe } from "@Hooks";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";

function Detail() {
  const { OneUser } = useListe();
  const history = useHistory();

  const redirect = (link: string) => () => {
    history.push(link);
    location.reload();
  };

  return (
    <>
      <Button
        onClick={redirect("/")}
        color="primary"
        className="btnStyle"
        startIcon={<ArrowBackIcon />}
      >
        Retour à la liste
      </Button>
      <div className="body">
        <Card className="root">
          <CardContent className="position">
            <img className="avatar" alt="Remy Sharp" src={OneUser.avatar} />
            <Typography variant="h5" component="h2">
              {OneUser.first_name} {OneUser.last_name}
            </Typography>
            <Typography className="pos" color="textSecondary">
              {OneUser.email}
            </Typography>
          </CardContent>
        </Card>
      </div>
    </>
  );
}

export default Detail;
