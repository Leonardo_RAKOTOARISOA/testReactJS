import { Button, Grid, Typography } from "@material-ui/core";
import "../../styles/detailRessource.scss";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useHistory } from "react-router-dom";
import { useRessource } from "@Hooks";

function DetailRessource() {
  const history = useHistory();

  const redirect = (link: string) => () => {
    history.push(link);
    location.reload();
  };

  const { OneRessource } = useRessource();

  return (
    <>
      <Button
        onClick={redirect("/")}
        color="primary"
        className="btnStyle"
        startIcon={<ArrowBackIcon />}
      >
        Retour à la liste
      </Button>
      <div className="position">
        <Grid container={true} direction="row" spacing={2}>
          <Grid item={true} md={3} sm={6} xs={12}>
            <Typography
              className="text"
              style={{ backgroundColor: `${OneRessource.color}` }}
            >
              Nom: {OneRessource.name}
            </Typography>
          </Grid>
          <Grid item={true} md={3} sm={6} xs={12}>
            <Typography className="textwhite">
              Valeur: {OneRessource.pantone_value}
            </Typography>
          </Grid>
          <Grid item={true} md={3} sm={6} xs={12}>
            <Typography className="textwhite">
              Année: {OneRessource.year}
            </Typography>
          </Grid>
        </Grid>
      </div>
    </>
  );
}

export default DetailRessource;
