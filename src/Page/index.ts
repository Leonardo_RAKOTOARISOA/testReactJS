// eslint-disable-next-line simple-import-sort/exports
import { lazy } from "react";

const Home = lazy(() => import("./Home"));
const Detail = lazy(() => import("./Detail"));
const DetailRessource = lazy(() => import("./DetailRessource"));

export { Detail, DetailRessource, Home };
