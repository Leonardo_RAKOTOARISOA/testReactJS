/* eslint-disable simple-import-sort/imports */
import { Detail, DetailRessource, Home } from "@Page";
import { Switch } from "react-router-dom";
import { ProtectedLazyRoute, Alert } from "@Common";
import { useAlert } from "@Hooks";

const Router = () => {
  const alert = useAlert();

  const handleClose = () => {
    alert.setCloseAlert();
  };

  return (
    <>
      <Switch>
        <ProtectedLazyRoute path="/" exact={true} component={Home} />
        <ProtectedLazyRoute path="/detail" exact={true} component={Detail} />
        <ProtectedLazyRoute
          path="/detailRessource"
          exact={true}
          component={DetailRessource}
        />
      </Switch>

      <Alert
        open={alert.open}
        handleClose={handleClose}
        severity={alert.severity}
        message={alert.message}
      />
    </>
  );
};

export default Router;
