import { callApi } from '@Utils';

export const createUser = async (data: any) => {
    return callApi(`/user`, 'POST', data);
};

export const update = async (data: any) => {
    return callApi(`/user/:id`, 'PUT', data);
};

export const getUser = async (page: number) => {
    return callApi(`/users?page=${page}`, 'GET');
};
