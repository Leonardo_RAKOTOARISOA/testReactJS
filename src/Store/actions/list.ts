/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable indent */
/* eslint-disable operator-linebreak */
/* eslint-disable import/prefer-default-export */
import { AppThunk } from "../store";

export const setList =
  (data: any): AppThunk =>
  (dispatch) => {
    dispatch({
      type: "LIST",
      payload: data,
    });
  };

export const setOneUser =
  (data: any): AppThunk =>
  (dispatch) => {
    dispatch({
      type: "ONEUSER",
      payload: data,
    });
  };
