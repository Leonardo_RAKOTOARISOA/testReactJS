/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable indent */
/* eslint-disable operator-linebreak */
/* eslint-disable import/prefer-default-export */
import { AppThunk } from "../store";

export const setListRess =
  (data: any): AppThunk =>
  (dispatch) => {
    dispatch({
      type: "LISTRESSOURCE",
      payload: data,
    });
  };

export const setOneRessource =
  (data: any): AppThunk =>
  (dispatch) => {
    dispatch({
      type: "ONERESSOURCE",
      payload: data,
    });
  };
