const INITIAL_STATE: any = {
  list: [],
  OneUser: {},
};

const apiList = (state = INITIAL_STATE, action: any): any => {
  switch (action.type) {
    case "LIST": {
      return { ...state, list: action.payload.data };
    }
    case "ONEUSER": {
      return { ...state, OneUser: action.payload.data };
    }
    default:
      return state;
  }
};

export default apiList;
