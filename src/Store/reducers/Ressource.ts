const INITIAL_STATE: any = {
  listRes: [],
  OneRessource: {},
};

const apiRessource = (state = INITIAL_STATE, action: any): any => {
  switch (action.type) {
    case "LISTRESSOURCE": {
      return { ...state, listRes: action.payload.data };
    }
    case "ONERESSOURCE": {
      return { ...state, OneRessource: action.payload.data };
    }
    default:
      return state;
  }
};

export default apiRessource;
