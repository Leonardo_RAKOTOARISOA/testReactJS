export interface ISnackBar {
  open: boolean;
  handleClose: () => void;
  severity?: "success" | "info" | "warning" | "error";
  message?: string;
}
