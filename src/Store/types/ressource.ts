export interface IRessource {
  id: number;
  name: string;
  year: number;
  color: string;
  pantone_value: string;
}

export interface IListRes {
  list: IRessource[];
}
