export interface IUserListe {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export interface IList {
  list: IUserListe[];
}
