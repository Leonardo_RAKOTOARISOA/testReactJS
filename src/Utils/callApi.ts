import axios from 'axios';

type IMethod = 'GET' | 'HEAD' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'OPTIONS'

export const callApi = async (url: string, method: IMethod, body?: any) => {

    try {

        return await axios({
            "url": url,
            "method": method,
            "data": body,
            "headers": {
                'Content-Type': 'application/json',
                'Authorization': sessionStorage.getItem('token')
            }
        });

    } catch (error: any) {

        if (error.message.includes('401')) {
            window.location.replace("/");
        }

        throw error;

    }
};
